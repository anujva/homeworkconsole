clear all;
close all;
clc;

filename = 'test_gray_question_1a.raw';
fileid = fopen(filename, 'r');

x = fread(fileid, [400,300]);
pixel = 0:255;
[his, val] = hist(x,pixel);