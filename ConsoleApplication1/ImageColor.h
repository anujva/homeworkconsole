#pragma once
class ImageColor{
private:
	int r;
	int g;
	int b;

public:
	ImageColor(void);
	~ImageColor(void);
	int getR();
	int getG();
	int getB();
	void setR(int _r);
	void setG(int _g);
	void setB(int _b);
};

