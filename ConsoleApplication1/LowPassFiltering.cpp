#include "LowPassFiltering.h"


LowPassFiltering::LowPassFiltering(void)
{
}

LowPassFiltering::LowPassFiltering(int type, ImageGray** ImageSet, int width, int height){
	define_filters();
	int filter[3][3];
	int weight;
	if(type == 9){
		for(int i =0; i<3; i++){
			for(int j =0; j<3; j++){
				filter[i][j] = filter_9[i][j];
			}
		}
		weight = 9;
	}else if(type == 10){
		for(int i =0; i<3; i++){
			for(int j =0; j<3; j++){
				filter[i][j] = filter_10[i][j];
			}
		}
		weight =10;
	}else if(type == 16){
		for(int i =0; i<3; i++){
			for(int j =0; j<3; j++){
				filter[i][j] = filter_16[i][j];
			}
		}
		weight =16;
	}else if(type == 25){
		//use filter5x5
		weight = 273;
	}

	//now that filter has the right values we need to process the entire image
	if(type != 25){	
		for(int i=0; i<height; i++){
			for(int j = 0; j<width; j++){
				int sum = 0;
				int row_index = 0, column_index = 0;
				for(int a=-1; a<2; a++){
					for(int b =-1; b <2; b++){
						if(a+i < 0 || a+i>(height-1) || j+b<0 || j+b>(width-1)){
							//sum = sum + filter[a+i][b+j]*ImageSet[i][j].getGrayscale();
							if(a+i<0){
								row_index = -1*(a+i);
							}else if(a+i > (height -1)){
								row_index =(height-1) - (a+i - (height-1));
							}else{
								row_index = a+i;
							}

							if(b+j<0){
								column_index = -1*(b+j);
							}else if(b+j > (width -1)){
								column_index =(width-1) - (b+j - (width-1));
							}else{
								column_index = b+j;
							}
							sum = sum + filter[a+1][b+1]*ImageSet[row_index][column_index].getGrayscale();
							continue;
						}
						sum = sum+ filter[a+1][b+1]*ImageSet[i+a][j+b].getGrayscale();
					}
				}
				sum = sum/weight;
				if(sum < 0){
					sum = 0;
				}else if(sum > 255){
					sum = 255;
				}
				ImageSet[i][j].setGrayscale(sum);
			}
		}
	}else{
		for(int i=0; i<height; i++){
			for(int j = 0; j<width; j++){
				int sum = 0;
				int row_index =0, column_index = 0;
				for(int a=-2; a<3; a++){
					for(int b =-2; b <3; b++){
						if(a+i < 0 || a+i>(height-1) || j+b<0 || j+b>(width-1)){
							//sum = sum + filter[a+i][b+j]*ImageSet[i][j].getGrayscale();
							if(a+i<0){
								row_index = -1*(a+i);
							}else if(a+i > (height -1)){
								row_index =(height-1) - (a+i - (height-1));
							}else{
								row_index = a+i;
							}

							if(b+j<0){
								column_index = -1*(b+j);
							}else if(b+j > (width -1)){
								column_index =(width-1) - (b+j - (width-1));
							}else{
								column_index = b+j;
							}
							sum = sum + filter_5x5[a+2][b+2]*ImageSet[row_index][column_index].getGrayscale();
							continue;
						}
						sum = sum+ filter_5x5[a+2][b+2]*ImageSet[i+a][j+b].getGrayscale();
					}
				}
				sum = sum/weight;
				if(sum < 0){
					sum = 0;
				}else if(sum > 255){
					sum = 255;
				}
				ImageSet[i][j].setGrayscale(sum);
			}
		}
	}
}

void LowPassFiltering::define_filters(){
	int arr9[9] = {1,1,1,1,1,1,1,1,1};
	int arr10[9] = {1,1,1,1,2,1,1,1,1};
	int arr16[9] = {1,2,1,2,4,2,1,2,1};

	int index =0;
	for(int i =0; i<3; i++){
		for(int j =0; j<3; j++){
			filter_9[i][j] = arr9[index];
			filter_10[i][j] = arr10[index];
			filter_16[i][j] = arr16[index];
			index++;
		}
	}

	int arr5x5[] = {1,4,7,4,1,4,16,26,16,4,7,26,41,26,7,4,16,26,16,4,1,4,7,4,1};
	index = 0;
	for(int i = 0; i<5; i++){
		for(int j = 0; j<5; j++){
			filter_5x5[i][j] = arr5x5[index];
			index++;
		}
	}
}

LowPassFiltering::~LowPassFiltering(void)
{
}
