#include <iostream>
#include <fstream>
#include "Image.h"
#include <list>
#include <cmath>

using namespace std;

int main(int argc, char *argv[]){
	fstream myfile;
	Image img(400, 300, "color");
	Image gray_img(400, 300, "gray");

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW1\\desk.raw", ios::in|ios::binary);
	int i = 0; int j = 0;
	while(!myfile.eof()){
		img.imgcolor[i][j].setR((int)myfile.get());
		img.imgcolor[i][j].setG((int)myfile.get());
		img.imgcolor[i][j].setB((int)myfile.get());
		j++;
		//cout<<i<<"  "<<j<<endl;
		if(j == 400){
			j = 0;
			i++;
		}
		if(i == 300) break;
	}
	//cout<<i<<"  "<<j<<endl;
	myfile.close();

	myfile.open("test.raw", ios::out|ios::binary);

	for(int i=0; i<300; i++){
		for(int j=0; j<400; j++){
			myfile.put(img.imgcolor[i][j].getR());
			myfile.put(img.imgcolor[i][j].getG());
			myfile.put(img.imgcolor[i][j].getB());
		}
	}

	myfile.close();

	for(int i = 0; i<300; i++){
		for(int j=0; j<400; j++){
			gray_img.imggray[i][j].setGrayscale((int)(0.21*(float)img.imgcolor[i][j].getB()+0.72*(float)img.imgcolor[i][j].getG()+0.07*(float)img.imgcolor[i][j].getR()));
		}
	}


	//just averaging
	Image just_AVG(400,300, "gray");
	for(int i = 0; i<300; i++){
		for(int j=0; j<400; j++){
			just_AVG.imggray[i][j].setGrayscale((img.imgcolor[i][j].getB()+img.imgcolor[i][j].getG()+(float)img.imgcolor[i][j].getR())/3);
		}
	}

	myfile.open("test_gray_question_1a.raw", ios::out|ios::binary);

	for(int i=0; i<300; i++){
		for(int j =0; j<400; j++){
			myfile.put(gray_img.imggray[i][j].getGrayscale());
		}
	}

	myfile.close();

	
	myfile.open("gray_justavg_1a.raw", ios::out|ios::binary);

	for(int i=0; i<300; i++){
		for(int j =0; j<400; j++){
			myfile.put(just_AVG.imggray[i][j].getGrayscale());
		}
	}

	myfile.close();
	int histogram[256];
	for(int i = 0; i<256; i++){
		histogram[i] = 0;
	}

	//uniform linear scaling code..
	//lets look at the histogram to be able to see what the image is going to be like.
	for(int i = 0; i<300; i++){
		for(int j=0; j<400; j++){
			histogram[gray_img.imggray[i][j].getGrayscale()]++;
		}
	}

	/*for(int i = 0; i<256; i++){
		cout<<i<<"    -->  "<<histogram[i]<<endl;
	}*/

	//find the minimum and maximum value of F (Fmin and Fmax) 
	int highest_val, highest_val_pos, least_val, least_val_pos;
	highest_val = histogram[0];
	highest_val_pos = 0;
	least_val = histogram[0];
	least_val_pos = 0;

	for(int i = 0; i<256; i++){
		if(highest_val < histogram[i]){
			highest_val = histogram[i];
			highest_val_pos = i;
		}

		if(least_val > histogram[i]){
			least_val = histogram[i];
			least_val_pos = i;
		}
	}

	//now that we have the least and the highest values of the histogram we need to decide what will be a good estimate for the Fmin and Fmax values.. 
	//one way to do this would be to get the clusters around the min and max values.. and just treat the cluster around the max as the one that we need to equalize.
	//two things that we need in a cluster point.. its histogram value, and second is its pixel value. lets create a class structure for that.. 

	class ClusterPoint{
	public:
		int histogram_val;
		int pixel_val;

		ClusterPoint(int _his, int _pix){
			histogram_val = _his;
			pixel_val = _pix;
		}

		int operator-(ClusterPoint C){
			int d;
			d = histogram_val - C.histogram_val;
			return d;
		}
	};

	//now we define the first two cluster points which will be the mean of the cluster.

	ClusterPoint cp_mean_high(highest_val, highest_val_pos);
	ClusterPoint cp_mean_low(least_val, least_val_pos);

	//now we need two linked lists where we can load all the points 
	list<ClusterPoint> cluster1;
	list<ClusterPoint> cluster2;

	for(int i = 0; i<256; i++){
		ClusterPoint temp(histogram[i], i);
		//find the distance from the cp_mean_high and cp_mean_low;
		//int dist1 = cp_mean_high - temp;
		//int dist2 = temp - cp_mean_low;
		//cout<<"Distances :  "<<dist1<<"    "<<dist2<<endl;
		//cout<<temp.histogram_val<<endl;
		if(temp.histogram_val>120){
			cluster1.push_back(temp);
		}else{
			cluster2.push_back(temp);
		}
	}

	//now in the cluster we will have to find the Fmin and Fmax we do that by looking at the max and then finding on either side values that are continuously decreasing.
	int Fmin, Fmax;
	list<ClusterPoint>::iterator itr;
	bool fmin_found = false;
	bool diff_updated = true;
	cout<<"the highest histogram value is : "<<cp_mean_high.histogram_val<<"  and the corresponding pixel value is: "<<cp_mean_high.pixel_val<<endl;
	while(!fmin_found){
		int diff = 0;
		while(diff_updated){	
			for(itr = cluster1.begin(); itr!= cluster1.end(); itr++){
				if((*itr).pixel_val == cp_mean_high.pixel_val - diff){
					diff++;
					diff_updated = true;
					break;
				}else{
					diff_updated = false;
				}
			}
		}
		cout<<endl<<--diff<<endl;
		fmin_found = true;
		Fmin = cp_mean_high.pixel_val - diff;
	}
	bool fmax_found = false;
	diff_updated = true;
	while(!fmax_found){
		int diff = 0;
		while(diff_updated){	
			for(itr = cluster1.begin(); itr!= cluster1.end(); itr++){
				if((*itr).pixel_val == cp_mean_high.pixel_val + diff){
					diff++;
					diff_updated = true;
					break;
				}else{
					diff_updated = false;
				}
			}
		}
		cout<<endl<<--diff<<endl;
		fmax_found = true;
		Fmax = cp_mean_high.pixel_val + diff;
	}

	cout<<"The value of fmin and fmax is found to be: "<<Fmin<<"   "<<Fmax<<endl;

	//Now to create a new image with the new values of pixels (linear scaling)
	Image img_linear_scale(400, 300, "gray");
	for(int i =0; i<300; i++){
		for(int j=0; j<400; j++){
			if(gray_img.imggray[i][j].getGrayscale() >=Fmin && gray_img.imggray[i][j].getGrayscale()<=Fmax){
					//cout<<"This is the new grayscale value after linear scaling: "<<(255.0/(float)(Fmax - Fmin))*((float)gray_img.imggray[i][j].getGrayscale() - (float)Fmin)<<endl;
				img_linear_scale.imggray[i][j].setGrayscale((255.0/(float)(Fmax - Fmin))*((float)gray_img.imggray[i][j].getGrayscale() - (float)Fmin));
			}else{
				img_linear_scale.imggray[i][j].setGrayscale(255);
			}
		}
	}
	
	myfile.open("test_linear_scale.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j =0; j<400; j++){
			myfile.put(img_linear_scale.imggray[i][j].getGrayscale());
		}
	}

	myfile.close();


	Image img_sqroot_scale(400, 300, "gray");
	for(int i =0; i<300; i++){
		for(int j=0; j<400; j++){
			if(gray_img.imggray[i][j].getGrayscale() >=Fmin && gray_img.imggray[i][j].getGrayscale()<=Fmax){
				double result = pow((((double)gray_img.imggray[i][j].getGrayscale() - (double)Fmin))/255,0.5);
				result = result * 255;
				if(result > 255){
					result = 255;
				}
				img_sqroot_scale.imggray[i][j].setGrayscale((int)result);
			}else{
				img_sqroot_scale.imggray[i][j].setGrayscale(255);
			}
		}
	}

	myfile.open("test_sqroot_scale.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j =0; j<400; j++){
			myfile.put(img_sqroot_scale.imggray[i][j].getGrayscale());
		}
	}

	myfile.close();


	//Histogram Equalization
	//we already have the histogram of the image.. we need to find the cumulative histogram now.. 
	int cdf[256];

	for(int i =0 ; i< 256; i++){
		cdf[i] = 0;
	}

	cdf[0] = histogram[0];

	for(int i =1 ; i<256; i++){
		cdf[i] = histogram[i] + cdf[i-1];
	}

	/*for(int i = 0; i<256; i++){
		cout<<i<<"    -->  "<<cdf[i]<<endl;
	}*/

	Image img_histeq_scale(400,300, "gray");
	//applying the formula
	for(int i = 0; i<300; i++){
		for(int j=0; j<400; j++){
			int curr_pixel = gray_img.imggray[i][j].getGrayscale();
			int pix_mapped  = (((float)cdf[curr_pixel] - (float)cdf[0])/(400.00*300.00 - (float)cdf[0]))*255; 
			if(pix_mapped > 255){
				pix_mapped = 255;
			}else if(pix_mapped < 0){
				pix_mapped = 0;
			}
			img_histeq_scale.imggray[i][j].setGrayscale(pix_mapped);
		}
	}

	myfile.open("test_histeq_scale.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j =0; j<400; j++){
			myfile.put(img_histeq_scale.imggray[i][j].getGrayscale());
		}
	}

	myfile.close();

	/*********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	*********************************************************************************DENOISING***********************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	**********************************************************************************************************************************************************************************************************
	*********************************************************************************************************************************************************************************************************/

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW1\\peppers_mixed.raw", ios::in|ios::binary);
	i = 0; j = 0;

	Image img_secondq(512, 512, "gray");
	Image img_secondq_original(512, 512, "gray");
	Image img_secondq_copy1(512, 512, "gray");
	Image img_secondq_copy2(512, 512, "gray");
	Image img_secondq_copy3(512, 512, "gray");
	Image img_secondq_copy4(512, 512, "gray");

	while(!myfile.eof()){
		img_secondq.imggray[i][j].setGrayscale((int)myfile.get());
		img_secondq_copy1.imggray[i][j].setGrayscale(img_secondq.imggray[i][j].getGrayscale());
		img_secondq_copy2.imggray[i][j].setGrayscale(img_secondq.imggray[i][j].getGrayscale());
		img_secondq_copy3.imggray[i][j].setGrayscale(img_secondq.imggray[i][j].getGrayscale());
		img_secondq_copy4.imggray[i][j].setGrayscale(img_secondq.imggray[i][j].getGrayscale());
		img_secondq_original.imggray[i][j].setGrayscale(img_secondq.imggray[i][j].getGrayscale());
		j++;
		//cout<<i<<"  "<<j<<endl;
		if(j == 512){
			j = 0;
			i++;
		}
		if(i == 512) break;
	}
	//cout<<i<<"  "<<j<<endl;
	myfile.close();
	

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW1\\peppers.raw", ios::in|ios::binary);
	i = 0; j = 0;

	Image img_secondq_wonoise(512, 512, "gray");

	while(!myfile.eof()){
		img_secondq_wonoise.imggray[i][j].setGrayscale(myfile.get());
		j++;
		if(j == 512){
			j = 0;
			i++;
		}
		if(i == 512) break;
	}
	myfile.close();
	
	myfile.open("test_pepper.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(img_secondq_original.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("test_pepper_withoutnoise.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(img_secondq_wonoise.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	img_secondq_wonoise.PSNR_Calculation(img_secondq);
	img_secondq_wonoise.SSIM_calculation(img_secondq, "original");
	//we have the image now.. we need to apply a filter median filter
	//img_secondq.mean_filtering_gray();
	
	img_secondq.median_filtering_gray();
	img_secondq.median_filtering_gray();
	img_secondq.median_filtering_gray();
	img_secondq.median_filtering_gray();
	img_secondq.filterImage(25);
	img_secondq.filterImage(25);
	img_secondq.filterImage(25);
	img_secondq.filterImage(25);
	

	myfile.open("test_pepper_2mean16_median4.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(img_secondq.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	//SSIM map generation and printing out is going to happen here
	Image SSIM_Map_wnoise = img_secondq_wonoise.SSIM_calculation(img_secondq_original, "This calculation is done with the original and the mixed noise image");
	Image SSIM_Map = img_secondq_wonoise.SSIM_calculation(img_secondq, "This calculation has been done with the original and the denoised image");

	myfile.open("SSIM_MAP_denoised.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(SSIM_Map.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("SSIM_MAP.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(SSIM_Map_wnoise.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	/*
	//the absolute error map generation and the MSE calculation
	Image AE_Map = img_secondq_wonoise.PSNR_Calculation(img_secondq_original);
	myfile.open("AE_MAP.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(AE_Map.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();
	*/
	img_secondq_wonoise.PSNR_Calculation(img_secondq);

	/*******************************************************************************************************************************************************************************************************************
	********************************************************************************************************************************************************************************************************************
	*********************************************************************************DIGITAL HISTOGRAM EQUILIZATION****************************************************************************************
	********************************************************************************************************************************************************************************************************************
	********************************************************************************************************************************************************************************************************************/
	
	//Digital Histogram Equalization NOT DENOISING HERE AT ALL
	ImageGray **return_img = gray_img.digital_histogram_eq_gray();
	myfile.open("DigitalHistogramEq.raw", ios::out|ios::binary);
	for(int i=0; i< 300; i++){
		for(int j=0; j< 400; j++){
			myfile.put(return_img[i][j].getGrayscale());
		}
	}
	myfile.close();


	/*******************************************************************************************************************************************************************************************************************
	********************************************************************************************************************************************************************************************************************
	*********************************************************************************Low Pass Filtering Techniques ****************************************************************************************
	********************************************************************************************************************************************************************************************************************
	********************************************************************************************************************************************************************************************************************/

	//filtering using the first simple filter
	img_secondq_copy1.filterImage(9);
	img_secondq_copy1.filterImage(9);
	img_secondq_copy1.filterImage(9);
	ImageGray** filtered_image_9 = img_secondq_copy1.filterImage(9);
	myfile.open("Filtered_img_9.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(filtered_image_9[i][j].getGrayscale());
		}
	}
	myfile.close();

	//filtering using the second weighted filter
	img_secondq_copy2.filterImage(10);
	img_secondq_copy2.filterImage(10);
	img_secondq_copy2.filterImage(10);
	ImageGray** filtered_image_10 = img_secondq_copy2.filterImage(10);
	myfile.open("Filtered_img_10.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(filtered_image_10[i][j].getGrayscale());
		}
	}
	myfile.close();


	//filtering using the third weighted filter
	img_secondq_copy3.filterImage(16);
	img_secondq_copy3.filterImage(16);
	img_secondq_copy3.filterImage(16);
	ImageGray** filtered_image_16 = img_secondq_copy3.filterImage(16);
	myfile.open("Filtered_img_16.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(filtered_image_16[i][j].getGrayscale());
		}
	}
	myfile.close();

	img_secondq_copy4.filterImage(25);
	img_secondq_copy4.filterImage(25);
	img_secondq_copy4.filterImage(25);
	ImageGray** filtered_image_25 = img_secondq_copy4.filterImage(25);
	myfile.open("Filtered_img_25.raw", ios::out|ios::binary);
	for(int i=0; i< 512; i++){
		for(int j=0; j< 512; j++){
			myfile.put(filtered_image_25[i][j].getGrayscale());
		}
	}
	myfile.close();


	//This part will deal with the edge maps
	//I will store both the images in an Image type object and then call the sobel operation on them

	Image img_girl_raw(256, 256, "gray");
	Image img_elaine_raw(256, 256, "gray");

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW1\\girl.raw", ios::in|ios::binary);
	i = 0; j=0;
	while(!myfile.eof()){
		img_girl_raw.imggray[i][j].setGrayscale((int)myfile.get());
		j++;
		//cout<<i<<"  "<<j<<endl;
		if(j == 256){
			j = 0;
			i++;
		}
		if(i == 256) break;
	}
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW1\\elaine.raw", ios::in|ios::binary);
	i = 0; j=0;
	while(!myfile.eof()){
		img_elaine_raw.imggray[i][j].setGrayscale((int)myfile.get());
		j++;
		//cout<<i<<"  "<<j<<endl;
		if(j == 256){
			j = 0;
			i++;
		}
		if(i == 256) break;
	}
	myfile.close();

	Image Sobel_map_elaine = img_elaine_raw.Sobel_Operator("full");
	Image Sobel_map_girl = img_girl_raw.Sobel_Operator("full");

	Image Sobel_map_elaine_gx = img_elaine_raw.Sobel_Operator("gx");
	Image Sobel_map_girl_gx = img_girl_raw.Sobel_Operator("gx");

	Image Sobel_map_elaine_gy = img_elaine_raw.Sobel_Operator("gy");
	Image Sobel_map_girl_gy = img_girl_raw.Sobel_Operator("gy");

	myfile.open("Elaine_edgemap.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_map_elaine.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("Elaine_edgemap_gx.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_map_elaine_gx.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("Elaine_edgemap_gy.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_map_elaine_gy.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("Girl_edgemap.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_map_girl.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("Girl_edgemap_gx.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_map_girl_gx.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("Girl_edgemap_gy.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_map_girl_gy.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	Image Log_edgemap_elaine = img_elaine_raw.Edge_Detector_LOG("Elaine.raw");
	myfile.open("Elaine_edgemap_LOG.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Log_edgemap_elaine.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();


	Image Log_edgemap_girl = img_girl_raw.Edge_Detector_LOG("Girl.raw");
	myfile.open("Girl_edgemap_LOG.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Log_edgemap_girl.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();


	Image Variance_map_elaine = img_elaine_raw.Variance_map("Elaine.raw");
	myfile.open("Elaine_Variance_Map.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Variance_map_elaine.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	Image Variance_map_girl = img_girl_raw.Variance_map("Elaine.raw");
	myfile.open("Girl_Variance_Map.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Variance_map_girl.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	/************************************************************************************************************************************************************************************************************
	*************************************************************************************************************************************************************************************************************
	************************************************************************************************EDGE THINNING******************************************************************************************
	*************************************************************************************************************************************************************************************************************
	************************************************************************************************************************************************************************************************************/
	Image Sobel_edge_thinned_elaine = Sobel_map_elaine.Edge_thinning(Sobel_map_elaine_gx, Sobel_map_elaine_gy);
	Image Sobel_edge_thinned_girl = Sobel_map_girl.Edge_thinning(Sobel_map_girl_gx, Sobel_map_girl_gy);

	myfile.open("Sobel_thinned_elaine.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_edge_thinned_elaine.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	myfile.open("Sobel_thinned_girl.raw", ios::out|ios::binary);
	for(int i=0; i< 256; i++){
		for(int j=0; j< 256; j++){
			myfile.put(Sobel_edge_thinned_girl.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	cout<<"To B or not to B"<<endl;

	return 1;
}