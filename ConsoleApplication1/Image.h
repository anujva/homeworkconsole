#pragma once
#include <iostream>
#include <string>
#include "ImageColor.h"
#include "ImageGray.h"
#include "Coordinates.h"
#include "LowPassFiltering.h"
#include <vector>
#include <algorithm>

using namespace std;

class Image{
private:
	int width;
	int height;
	string ImageType;
	LowPassFiltering *filter;

public:
	Image(void);
	~Image(void);
	Image(int _width, int _height, string ImageType);
	int getHeight();
	int getWidth();

	ImageColor **imgcolor;
	ImageGray **imggray;

	void median_filtering_gray();
	void mean_filtering_gray();

	ImageGray** digital_histogram_eq_gray();
	ImageGray** filterImage(int type);

	Image SSIM_calculation(Image B, string str);

	Image PSNR_Calculation(Image B);

	Image Sobel_Operator(string which_edge);

	Image Edge_Detector_LOG(string str);

	Image Variance_map(string str);

	Image Edge_thinning(Image Sobel_edgemap_gx, Image Sobel_edgemap_gy);
};

