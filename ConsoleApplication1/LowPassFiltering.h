#pragma once
#include<iostream>

#include "ImageGray.h"

class LowPassFiltering
{
private:
	int filter_9[3][3];
	int filter_10[3][3];
	int filter_16[3][3];

	int filter_5x5[5][5];

public:
	LowPassFiltering(void);
	~LowPassFiltering(void);
	LowPassFiltering(int type, ImageGray** ImageSet, int width, int height);
	void define_filters();
};

